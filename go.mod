module gitlab.com/etke.cc/linkpearl

go 1.21

toolchain go1.22.1

require (
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/rs/zerolog v1.33.0
	go.mau.fi/util v0.6.0
	maunium.net/go/mautrix v0.19.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/tidwall/gjson v1.17.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/yuin/goldmark v1.7.4 // indirect
	golang.org/x/crypto v0.25.0 // indirect
	golang.org/x/exp v0.0.0-20240719175910-8a7402abbf56 // indirect
	golang.org/x/net v0.27.0 // indirect
	golang.org/x/sys v0.22.0 // indirect
)
